// Test_BoS3.cpp : Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"

int feld = -1;
int sp = 1;
int counter = 0;

int array6[6] = { 0,2,4,6,8,10 };
int array5[5] = { 23, 25, 27, 29, 31 };
int array4[4] = { 46, 48, 50, 52 };
int array3[3] = { 69, 71, 73 };
int array2[2] = { 92, 94 };
int array1[1] = { 115 };

void faerbeStreichholz(int feldnr);
void reihe(int array[], int laenge);
void isgameover();

int _tmain(int argc, _TCHAR* argv[])
{
	int groessex = 11;
	int	groessey = 11;
	int x;
	int y;
	int counter = 0;
	int i;
	int sp = 1;

	loeschen();
	rahmen(RED);
	flaeche(BLACK);
	groesse(groessex, groessey);
	formen("|");

	//Spielbrett färben

	for (i = 0; i < 121; i++)
	{
		farbe(i, BLACK);
	}
	//Spielbrett
	for (y = 0; y < 12; y += 2)
	{

		for (x = 0 + counter; x < 12 - counter; x += 2)
		{
			farbe2(x, y, RED);
		}
		counter++;
		farbe(110, GREEN);
		form(110, "s");
		form(99, "s");
	}

	printf("Das Spielbrett ist geladen! Spieler 1 faengt an.\n");

	//abfrage für das klicken
	for (;;)
	{
		char *a = abfragen();
		if (a[0] == '#')
		{
			sscanf_s(a, "# %d", &feld);
			
			for (int k = 0; k < 6; k++) {
			
				switch (k) {
				case 0:
						
					if (array1[k] == feld)
					{
						faerbeStreichholz(array1[k]);
						reihe(array1, 1);

						break;
					}
					
					if (array2[k] == feld)
					{
						faerbeStreichholz(array2[k]);
						reihe(array2, 2);

						break;
					}

					if (array3[k] == feld)
					{
						faerbeStreichholz(array3[k]);
						reihe(array3, 3);

						break;
					}

					if (array4[k] == feld)
					{
						faerbeStreichholz(array4[k]);
						reihe(array4, 4);

						break;
					}

					if (array5[k] == feld)
					{
						faerbeStreichholz(array5[k]);
						reihe(array5, 5);

						break;
					}

					if (array6[k] == feld)
					{
						faerbeStreichholz(array6[k]);
						reihe(array6, 6);

						break;
					}

					break;

				case 1:

					if (array2[k] == feld)
					{
						faerbeStreichholz(array2[k]);
						reihe(array2, 2);

						break;
					}

					if (array3[k] == feld)
					{
						faerbeStreichholz(array3[k]);
						reihe(array3, 3);

						break;
					}

					if (array4[k] == feld)
					{
						faerbeStreichholz(array4[k]);
						reihe(array4, 4);

						break;
					}

					if (array5[k] == feld)
					{
						faerbeStreichholz(array5[k]);
						reihe(array5, 5);

						break;
					}

					if (array6[k] == feld)
					{
						faerbeStreichholz(array6[k]);
						reihe(array6, 6);

						break;
					}

					break;

				case 2:

					if (array3[k] == feld)
					{
						faerbeStreichholz(array3[k]);
						reihe(array3, 3);

						break;
					}

					if (array4[k] == feld)
					{
						faerbeStreichholz(array4[k]);
						reihe(array4, 4);

						break;
					}

					if (array5[k] == feld)
					{
						faerbeStreichholz(array5[k]);
						reihe(array5, 5);

						break;
					}

					if (array6[k] == feld)
					{
						faerbeStreichholz(array6[k]);
						reihe(array6, 6);

						break;
					}

					break;

				case 3:

					if (array4[k] == feld)
					{
						faerbeStreichholz(array4[k]);
						reihe(array4, 4);

						break;
					}

					if (array5[k] == feld)
					{
						faerbeStreichholz(array5[k]);
						reihe(array5, 5);

						break;
					}

					if (array6[k] == feld)
					{
						faerbeStreichholz(array6[k]);
						reihe(array6, 6);

						break;
					}

					break;

				case 4:

					if (array5[k] == feld)
					{
						faerbeStreichholz(array5[k]);
						reihe(array5, 5);

						break;
					}

					if (array6[k] == feld)
					{
						faerbeStreichholz(array6[k]);
						reihe(array6, 6);

						break;
					}

					break;

				case 5:

					if (array6[k] == feld)
					{
						faerbeStreichholz(array6[k]);
						reihe(array6, 6);

						break;
					}

					break;

				default:
					break;
				}
			
			}

		}

	}

	return 0;
}

void faerbeStreichholz(int feldnr) {

	farbe(feldnr, BLACK);

	counter++;

	isgameover();

	}

void reihe(int array[], int laenge) {

	for (bool ende = false; ende == false;)
	{
		char *a = abfragen();
		if (a[0] == '#')
		{
			sscanf_s(a, "# %d", &feld);

			for (int i = 0; i < laenge; i++) {

				if (array[i] == feld) {

					faerbeStreichholz(array[i]);

					break;
				}else if (feld == 110 && sp == 1)
				{
					farbe(110, BLUE);
					printf("Spieler 2 ist dran!\n");
					sp = 2;

					ende = true;
					break;
				}
				else if (feld == 110 && sp == 2)
				{
					farbe(110, GREEN);
					printf("Spieler 1 ist dran!\n");
					sp = 1;

					ende = true;
					break;

				}
			}

		}

	}

}

void isgameover() {

	if (counter >= 21) {
	
		printf("\n \n Spieler %i hat verloren.", sp);

		for (int i = 0; i <= 120;  i++) {
		
			farbe(i, BLACK);
		
		}
	}
}