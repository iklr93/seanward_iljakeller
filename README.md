### Das Nim-Spiel

Das Nim-Spiel ist ein Spiel für zwei Spieler, die versuchen sich gegenseitig zu
überlisten. Als Spielobjekte werden meist einfache Streichhölzer verwendet. Wir 
verwenden hierfür Board-of-Symbols zur Darstellung dieser Streichhölzer. Die 
Streichhölzer werden in Pyramidenform in mehreren Reihen angelegt. Beide Spieler
ziehen abwechselnd eine beliebige Anzahl von Streichhölzern einer Reihe.

Pro Zug darf sich der Spieler für eine beliebige Reihe entscheiden, kann die 
Reihe im nächsten Zug aber wechseln. Ziel des Spiels ist es, NICHT das letzte 
Streichholz zu ziehen, ansonsten verliert man.

Durch das einfache Spielprinzip, ist das Nim-Spiel eine beliebtes 
Zeitvertreibspiel, welches Vorrausdenken, List und Konzentration fördern kann.

#####Probleme, die auftreten können.  
* Visuelle Darstellung mit BoS  
* Überprüfung der legalen Spielzüge


### Ausführung des Spiels

Für die Ausführung des Spiels werden die BoS-Dateien vom Herrn Euler benötigt

Diese können Sie mit Benutzeranleitung [hier](https://github.com/stephaneuler/board-of-symbols) finden.

Nehmen Sie dann die Nimspiel.cpp und fügen Sie ihrer Entwicklungsumgebung bei.  
  
Führen Sie den jserver und die Nimspiel.cpp aus.
